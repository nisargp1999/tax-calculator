﻿namespace TaxCalculatorWinForm
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.instruction = new System.Windows.Forms.Label();
            this.userInput = new System.Windows.Forms.TextBox();
            this.EnterButton = new System.Windows.Forms.Button();
            this.Done = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.total = new System.Windows.Forms.Label();
            this.thirtySeven = new System.Windows.Forms.Label();
            this.thirtyFive = new System.Windows.Forms.Label();
            this.thirtyTwo = new System.Windows.Forms.Label();
            this.twentyFour = new System.Windows.Forms.Label();
            this.twentyTwo = new System.Windows.Forms.Label();
            this.twelve = new System.Windows.Forms.Label();
            this.ten = new System.Windows.Forms.Label();
            this.percentGrossIncome = new System.Windows.Forms.Label();
            this.percentAdjustedText = new System.Windows.Forms.Label();
            this.totalDeductionsText = new System.Windows.Forms.Label();
            this.grossIncomeText = new System.Windows.Forms.Label();
            this.help = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // instruction
            // 
            this.instruction.AutoSize = true;
            this.instruction.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.instruction.Location = new System.Drawing.Point(201, 45);
            this.instruction.Name = "instruction";
            this.instruction.Size = new System.Drawing.Size(0, 31);
            this.instruction.TabIndex = 0;
            this.instruction.Click += new System.EventHandler(this.label1_Click);
            // 
            // userInput
            // 
            this.userInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.userInput.Location = new System.Drawing.Point(313, 102);
            this.userInput.Name = "userInput";
            this.userInput.Size = new System.Drawing.Size(202, 38);
            this.userInput.TabIndex = 1;
            this.userInput.TextChanged += new System.EventHandler(this.userInput_TextChanged);
            // 
            // EnterButton
            // 
            this.EnterButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EnterButton.Location = new System.Drawing.Point(580, 102);
            this.EnterButton.Name = "EnterButton";
            this.EnterButton.Size = new System.Drawing.Size(95, 38);
            this.EnterButton.TabIndex = 2;
            this.EnterButton.Text = "Enter";
            this.EnterButton.UseVisualStyleBackColor = true;
            this.EnterButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // Done
            // 
            this.Done.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Done.Location = new System.Drawing.Point(313, 189);
            this.Done.Name = "Done";
            this.Done.Size = new System.Drawing.Size(202, 40);
            this.Done.TabIndex = 3;
            this.Done.Text = "Done";
            this.Done.UseVisualStyleBackColor = true;
            this.Done.Click += new System.EventHandler(this.Done_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.total);
            this.groupBox1.Controls.Add(this.thirtySeven);
            this.groupBox1.Controls.Add(this.thirtyFive);
            this.groupBox1.Controls.Add(this.thirtyTwo);
            this.groupBox1.Controls.Add(this.twentyFour);
            this.groupBox1.Controls.Add(this.twentyTwo);
            this.groupBox1.Controls.Add(this.twelve);
            this.groupBox1.Controls.Add(this.ten);
            this.groupBox1.Controls.Add(this.percentGrossIncome);
            this.groupBox1.Controls.Add(this.percentAdjustedText);
            this.groupBox1.Controls.Add(this.totalDeductionsText);
            this.groupBox1.Controls.Add(this.grossIncomeText);
            this.groupBox1.Location = new System.Drawing.Point(22, 263);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(771, 154);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // total
            // 
            this.total.AutoSize = true;
            this.total.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.total.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.total.Location = new System.Drawing.Point(560, 103);
            this.total.Name = "total";
            this.total.Size = new System.Drawing.Size(0, 17);
            this.total.TabIndex = 11;
            // 
            // thirtySeven
            // 
            this.thirtySeven.AutoSize = true;
            this.thirtySeven.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.thirtySeven.ForeColor = System.Drawing.Color.Navy;
            this.thirtySeven.Location = new System.Drawing.Point(560, 78);
            this.thirtySeven.Name = "thirtySeven";
            this.thirtySeven.Size = new System.Drawing.Size(0, 17);
            this.thirtySeven.TabIndex = 10;
            // 
            // thirtyFive
            // 
            this.thirtyFive.AutoSize = true;
            this.thirtyFive.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.thirtyFive.ForeColor = System.Drawing.Color.RoyalBlue;
            this.thirtyFive.Location = new System.Drawing.Point(560, 54);
            this.thirtyFive.Name = "thirtyFive";
            this.thirtyFive.Size = new System.Drawing.Size(0, 17);
            this.thirtyFive.TabIndex = 9;
            // 
            // thirtyTwo
            // 
            this.thirtyTwo.AutoSize = true;
            this.thirtyTwo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.thirtyTwo.ForeColor = System.Drawing.Color.DodgerBlue;
            this.thirtyTwo.Location = new System.Drawing.Point(560, 29);
            this.thirtyTwo.Name = "thirtyTwo";
            this.thirtyTwo.Size = new System.Drawing.Size(0, 17);
            this.thirtyTwo.TabIndex = 8;
            // 
            // twentyFour
            // 
            this.twentyFour.AutoSize = true;
            this.twentyFour.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.twentyFour.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.twentyFour.Location = new System.Drawing.Point(363, 103);
            this.twentyFour.Name = "twentyFour";
            this.twentyFour.Size = new System.Drawing.Size(0, 17);
            this.twentyFour.TabIndex = 7;
            // 
            // twentyTwo
            // 
            this.twentyTwo.AutoSize = true;
            this.twentyTwo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.twentyTwo.ForeColor = System.Drawing.Color.MediumTurquoise;
            this.twentyTwo.Location = new System.Drawing.Point(363, 78);
            this.twentyTwo.Name = "twentyTwo";
            this.twentyTwo.Size = new System.Drawing.Size(0, 17);
            this.twentyTwo.TabIndex = 6;
            // 
            // twelve
            // 
            this.twelve.AutoSize = true;
            this.twelve.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.twelve.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.twelve.Location = new System.Drawing.Point(363, 54);
            this.twelve.Name = "twelve";
            this.twelve.Size = new System.Drawing.Size(0, 17);
            this.twelve.TabIndex = 5;
            // 
            // ten
            // 
            this.ten.AutoSize = true;
            this.ten.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ten.ForeColor = System.Drawing.Color.Turquoise;
            this.ten.Location = new System.Drawing.Point(363, 29);
            this.ten.Name = "ten";
            this.ten.Size = new System.Drawing.Size(0, 17);
            this.ten.TabIndex = 4;
            // 
            // percentGrossIncome
            // 
            this.percentGrossIncome.AutoSize = true;
            this.percentGrossIncome.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.percentGrossIncome.ForeColor = System.Drawing.Color.MediumSpringGreen;
            this.percentGrossIncome.Location = new System.Drawing.Point(6, 103);
            this.percentGrossIncome.Name = "percentGrossIncome";
            this.percentGrossIncome.Size = new System.Drawing.Size(0, 17);
            this.percentGrossIncome.TabIndex = 3;
            // 
            // percentAdjustedText
            // 
            this.percentAdjustedText.AutoSize = true;
            this.percentAdjustedText.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.percentAdjustedText.ForeColor = System.Drawing.Color.SpringGreen;
            this.percentAdjustedText.Location = new System.Drawing.Point(6, 78);
            this.percentAdjustedText.Name = "percentAdjustedText";
            this.percentAdjustedText.Size = new System.Drawing.Size(0, 17);
            this.percentAdjustedText.TabIndex = 2;
            // 
            // totalDeductionsText
            // 
            this.totalDeductionsText.AutoSize = true;
            this.totalDeductionsText.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalDeductionsText.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.totalDeductionsText.Location = new System.Drawing.Point(6, 54);
            this.totalDeductionsText.Name = "totalDeductionsText";
            this.totalDeductionsText.Size = new System.Drawing.Size(0, 17);
            this.totalDeductionsText.TabIndex = 1;
            // 
            // grossIncomeText
            // 
            this.grossIncomeText.AutoSize = true;
            this.grossIncomeText.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grossIncomeText.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.grossIncomeText.Location = new System.Drawing.Point(6, 29);
            this.grossIncomeText.Name = "grossIncomeText";
            this.grossIncomeText.Size = new System.Drawing.Size(0, 17);
            this.grossIncomeText.TabIndex = 0;
            // 
            // help
            // 
            this.help.AutoSize = true;
            this.help.ForeColor = System.Drawing.Color.Red;
            this.help.Location = new System.Drawing.Point(310, 152);
            this.help.Name = "help";
            this.help.Size = new System.Drawing.Size(0, 13);
            this.help.TabIndex = 5;
            this.help.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Enabled = false;
            this.pictureBox1.Image = global::TaxCalculatorWinForm.Properties.Resources._42430_200;
            this.pictureBox1.Location = new System.Drawing.Point(31, 102);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(273, 127);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(825, 457);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.help);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.Done);
            this.Controls.Add(this.EnterButton);
            this.Controls.Add(this.userInput);
            this.Controls.Add(this.instruction);
            this.Name = "Form1";
            this.Text = "Federal Income Tax Calculator";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label instruction;
        private System.Windows.Forms.TextBox userInput;
        private System.Windows.Forms.Button EnterButton;
        private System.Windows.Forms.Button Done;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label thirtySeven;
        private System.Windows.Forms.Label thirtyFive;
        private System.Windows.Forms.Label thirtyTwo;
        private System.Windows.Forms.Label twentyFour;
        private System.Windows.Forms.Label twentyTwo;
        private System.Windows.Forms.Label twelve;
        private System.Windows.Forms.Label ten;
        private System.Windows.Forms.Label percentGrossIncome;
        private System.Windows.Forms.Label percentAdjustedText;
        private System.Windows.Forms.Label totalDeductionsText;
        private System.Windows.Forms.Label grossIncomeText;
        private System.Windows.Forms.Label total;
        private System.Windows.Forms.Label help;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

