﻿
//Citation: Code for calculating taxes comes from Eric Charnesky
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TaxCalculatorWinForm
{
    public partial class Form1 : Form
    {
        Boolean doneWithIncome = false;
        Boolean doneWithDeductions = false;
        double income = 0;
        double grossIncome = 0;
        double totalDeductions = 0;
        double deduction = 0;
        public Form1()
        {


            InitializeComponent();

            instruction.Text = "Please enter your income one by one:";
            help.Text = "Press enter each time for the same value type\nPress done when you want to move on";

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void userInput_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!doneWithIncome)
            {
                income = Convert.ToDouble(userInput.Text);
                grossIncome += income;
            }
            else if (!doneWithDeductions)
            {
                deduction = Convert.ToDouble(userInput.Text);
                totalDeductions += deduction;
            }
            userInput.Text = null;
        }

        private void Done_Click(object sender, EventArgs e)
        {
            if (!doneWithIncome)
            {
                doneWithIncome = true;
                instruction.Text = "Please enter your deductions one by one:";
            }
            else if (!doneWithDeductions)
            {
                doneWithDeductions = true;
                instruction.Text = "Thanks, I will take it from here.";
                var taxInfo = new TaxesFor2018(grossIncome, totalDeductions);

                grossIncomeText.Text = $"Gross Income: ${grossIncome.ToString()}";
                totalDeductionsText.Text = $"Total Deductions: ${taxInfo.TotalDeductions.ToString()}";
                ten.Text = $"Taxes owed at 10%: ${taxInfo.Taxes_owed_at_10_percent.ToString()}";
                twelve.Text = $"Taxes owed at 12%: ${taxInfo.Taxes_owed_at_12_percent.ToString()}";
                twentyTwo.Text = $"Taxes owed at 22%: ${taxInfo.Taxes_owed_at_22_percent.ToString()}";
                twentyFour.Text = $"Taxes owed at 24%: ${taxInfo.Taxes_owed_at_24_percent.ToString()}";
                thirtyTwo.Text = $"Taxes owed at 32%: ${taxInfo.Taxes_owed_at_32_percent.ToString()}";
                thirtyFive.Text = $"Taxes owed at 35%: ${taxInfo.Taxes_owed_at_35_percent.ToString()}";
                thirtySeven.Text = $"Taxes owed at 37%: ${taxInfo.Taxes_owed_at_37_percent.ToString()}";
                percentAdjustedText.Text = $"Taxes owed at a % of Adjusted Gross income: {taxInfo.TaxesOwedAsPercentageOfAdjustedGrossIncome.ToString("0.00")}%";
                percentGrossIncome.Text = $"Taxes owed at a % of Gross Income: {taxInfo.TaxesOwedAsPercentageOfGrossIncome.ToString("0.00")}%";

                double totalTax = (taxInfo.TaxesOwedAsPercentageOfAdjustedGrossIncome / 100) * taxInfo.AdjustedGrossIncome;
                total.Text = $"Total Taxes: ${totalTax.ToString("0")}";

            }
            else
            {
                instruction.Text = "Yeah, no need to keep pressing done.";
            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }

    public class TaxesFor2018
    {
        static int[] Tax_Bracket_Floors = { 600_000, 400_000, 315_000, 165_000, 77_400, 19_050, 0 };
        static double[] Tax_Bracket_Rates = { .37, .35, .32, .24, .22, .12, .1 };

        public double Taxes_owed_at_37_percent { get; private set; }
        public double Taxes_owed_at_35_percent { get; private set; }
        public double Taxes_owed_at_32_percent { get; private set; }
        public double Taxes_owed_at_24_percent { get; private set; }
        public double Taxes_owed_at_22_percent { get; private set; }
        public double Taxes_owed_at_12_percent { get; private set; }
        public double Taxes_owed_at_10_percent { get; private set; }

        public double TotalTaxesOwed { get; private set; }

        public double TaxesOwedAsPercentageOfGrossIncome { get; private set; }
        public double TaxesOwedAsPercentageOfAdjustedGrossIncome { get; private set; }

        private double GrossIncome;
        public double TotalDeductions { get; private set; }
        public double AdjustedGrossIncome;

        public TaxesFor2018(double GrossIncome, double TotalDeductions)
        {
            this.GrossIncome = GrossIncome;
            if (TotalDeductions < 24_000)
            {
                TotalDeductions = 24_000;
            }
            this.TotalDeductions = TotalDeductions;
            AdjustedGrossIncome = GrossIncome - TotalDeductions;
            double incomeToBeTaxed = AdjustedGrossIncome;
            TotalTaxesOwed = 0;

            double[] taxes_at_brackets = { 0, 0, 0, 0, 0, 0, 0 };

            for (int index = 0; index < Tax_Bracket_Floors.Length; index++)
            {
                if (incomeToBeTaxed > Tax_Bracket_Floors[index])
                {
                    taxes_at_brackets[index] = (incomeToBeTaxed - Tax_Bracket_Floors[index]) * Tax_Bracket_Rates[index];
                    incomeToBeTaxed = Tax_Bracket_Floors[index];
                    TotalTaxesOwed += taxes_at_brackets[index];
                }
            }

            Taxes_owed_at_37_percent = taxes_at_brackets[0];
            Taxes_owed_at_35_percent = taxes_at_brackets[1];
            Taxes_owed_at_32_percent = taxes_at_brackets[2];
            Taxes_owed_at_24_percent = taxes_at_brackets[3];
            Taxes_owed_at_22_percent = taxes_at_brackets[4];
            Taxes_owed_at_12_percent = taxes_at_brackets[5];
            Taxes_owed_at_10_percent = taxes_at_brackets[6];

            TaxesOwedAsPercentageOfGrossIncome = (TotalTaxesOwed / GrossIncome) * 100;
            TaxesOwedAsPercentageOfAdjustedGrossIncome = (TotalTaxesOwed / AdjustedGrossIncome) * 100;

        }
    }
}
